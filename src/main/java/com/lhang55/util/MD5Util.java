package com.lhang55.util;

import com.lhang55.app.lambda.WorkerInterface;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/8/23 21:11
 */
public class MD5Util {

    private static final String TAG = MD5Util.class.getName();
    private static MessageDigest mMessageDigest;

    private static char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    static {
        try {
            mMessageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            //Log.e("", TAG + " MessageDigest.getInstance() failed :" + e.toString());
        }
    }

    public final static String MD5(String s) {

        try {
            byte[] btInput = s.getBytes();
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) {
//        System.out.println(MD5Util.MD5("20121221"));
//        System.out.println(MD5Util.MD5("加密"));
//        System.out.println(getMD5("654321"));
//        System.out.println(str2HexStr("Hello"));
        WorkerInterface workerInterface = (() -> {
            System.out.println("Hello World");
        });
    }

    public static void execute(WorkerInterface worker) {
        worker.doSomeWork();
    }

    public static String str2HexStr(String str) {
        char[] chars = "0123456789ABCDEF".toCharArray();
        StringBuilder sb = new StringBuilder("");
        byte[] bs = str.getBytes();
        int bit;
        for (int i = 0; i < bs.length; i++) {
            bit = (bs[i] & 0x0f0) >> 4;
            sb.append(chars[bit]);
            bit = bs[i] & 0x0f;
            sb.append(chars[bit]);
            // sb.append(' ');
        }
        return sb.toString().trim();
    }


    /**
     * 对字符串md5加密，lms系统中计算用户口令
     *
     * @param str
     * @return
     */
    public static String getMD5(String str) {
        try {
            // 生成一个MD5加密计算摘要
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 计算md5函数
            md.update(str.getBytes());
            // digest()最后确定返回md5 hash值，返回值为8为字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
            // BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
            return new BigInteger(1, md.digest()).toString(16);
        } catch (Exception e) {
            e.printStackTrace();
//            throw new SpeedException("MD5加密出现错误");
        }
        return null;
    }

    public static String getFileMD5String(String filePath) throws IOException {
        InputStream fis;
        fis = new FileInputStream(filePath);
        byte[] buf = new byte[1024];
        int numRead = 0;
        while ((numRead = fis.read(buf)) != -1) {
            mMessageDigest.update(buf, 0, numRead);
        }
        fis.close();
        return bufferToHex(mMessageDigest.digest());
    }

    public static String getFileMD5String(InputStream fis) throws IOException {
        byte[] buf = new byte[1024];
        int numRead = 0;
        while ((numRead = fis.read(buf)) != -1) {
            mMessageDigest.update(buf, 0, numRead);
        }
        fis.close();
        return bufferToHex(mMessageDigest.digest());
    }

    private static String bufferToHex(byte bytes[]) {
        return bufferToHex(bytes, 0, bytes.length);
    }

    private static String bufferToHex(byte bytes[], int m, int n) {
        StringBuffer stringbuffer = new StringBuffer(2 * n);
        int k = m + n;
        for (int l = m; l < k; l++) {
            appendHexPair(bytes[l], stringbuffer);
        }
        return stringbuffer.toString();
    }

    private static void appendHexPair(byte bt, StringBuffer stringBuffer) {
        char c0 = hexDigits[(bt & 0xf0) >> 4];
        char c1 = hexDigits[bt & 0xf];
        stringBuffer.append(c0);
        stringBuffer.append(c1);
    }
}
