package com.lhang55.app.config;


public class SgtPeppers implements CompactDisc {

    private String title = "sgt. People's Lonely Hearts Club Band";

    private String artist = "The Beatles";

    @Override
    public void play() {
        System.out.println("Playing " + title + " by " + artist);
    }
}
