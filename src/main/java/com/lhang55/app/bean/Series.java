package com.lhang55.app.bean;

import java.util.List;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/4/2 9:54
 */
public class Series<T> {
    private String name;
    private String type;
    private List<T> data;
    private Label label;
    private String left;
    private String right;
    private String top;
    private String bottom;

    public Series(String name, String type, List<T> data, Label label) {
        this.name = name;
        this.type = type;
        this.data = data;
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    public String getTop() {
        return top;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public String getBottom() {
        return bottom;
    }

    public void setBottom(String bottom) {
        this.bottom = bottom;
    }
}
