package com.lhang55.app.bean;


import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/4/2 9:38
 */
public class EchartData {

    public List<String> legend = new ArrayList<String>();// 数据分组
    public List<String> category = new ArrayList<String>();// 横坐标
    public List<Series> series = new ArrayList<Series>();// 纵坐标

    public EchartData(List<String> legend, List<String> category, List<Series> series) {
        this.legend = legend;
        this.category = category;
        this.series = series;
    }
}
