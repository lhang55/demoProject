package com.lhang55.app.bean;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "user_id", length = 50, nullable = false)
    private String userId;
    @Column(name = "user_name", length = 100)
    private String userName;
    @Column(length = 20)
    private String phone;
    @Column(length = 50)
    private String email;
    @Column(length = 20)
    private String password;
    @Column(length = 500)
    private String description;
    @Column(columnDefinition = "TINYINT", length = 1)
    private Integer status;
    @Column(name = "create_user", length = 50, nullable = false, insertable = true, updatable = false)
    private String createUser;
    @Temporal(TemporalType.DATE)
    @Column(name = "create_time", columnDefinition = "DATETIME")
    private Date createTime;
    @Column(name = "update_User", length = 50)
    private String updateUser;
    @Temporal(TemporalType.DATE)
    @Column(name = "update_time", columnDefinition = "DATETIME")
    private Date updateTime;

}
