package com.lhang55.app.bean;

import lombok.Data;

import javax.persistence.Column;
import java.util.Date;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/8/20 12:16
 */
@Data
public class HumorBlog {
    @Column
    private Long resourceId;

    @Column
    private String content;

    @Column
    private Date createdate;
}
