package com.lhang55.app.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/4/2 18:25
 */
public class TreeNode {
    private String id;

    private String pid;

    private String name;

    private String value;

    private List<TreeNode> children = new ArrayList<TreeNode>();

    public TreeNode(String name) {
        this.name = name;
    }

    public TreeNode(String id, String pid, String name, String value) {
        this.id = id;
        this.pid = pid;
        this.name = name;
        this.value = value;
    }

    /**
     * 递归添加节点
     * @param node
     */
    public void add(TreeNode node) {
        if ("0".equals(node.pid)) {
            this.children.add(node);
        } else if (node.pid.equals(this.id)) {
            this.children.add(node);
        } else {
            for (TreeNode tmp_node : children) {
                tmp_node.add(node);
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }
}
