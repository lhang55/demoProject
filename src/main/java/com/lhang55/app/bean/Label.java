package com.lhang55.app.bean;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/4/3 10:10
 */
public class Label {
    public String position;

    public String verticalAlign;

    public String align;

    public String fontSize;

    public Label() {

    }

    public Label(String position, String verticalAlign, String align, String fontSize) {
        this.position = position;
        this.verticalAlign = verticalAlign;
        this.align = align;
        this.fontSize = fontSize;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getVerticalAlign() {
        return verticalAlign;
    }

    public void setVerticalAlign(String verticalAlign) {
        this.verticalAlign = verticalAlign;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public String getFontSize() {
        return fontSize;
    }

    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }
}
