package com.lhang55.app.bean;

import javax.persistence.Column;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/4/2 18:04
 */
public class Tooltip {
    @Column
    private String trigger;

    private String triggerOn;

    public Tooltip(String trigger, String triggerOn) {
        this.trigger = trigger;
        this.triggerOn = triggerOn;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    public String getTriggerOn() {
        return triggerOn;
    }

    public void setTriggerOn(String triggerOn) {
        this.triggerOn = triggerOn;
    }
}
