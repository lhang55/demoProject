package com.lhang55.app.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/4/2 18:03
 */
public class EchartsTree {

    public Tooltip tooltip;

    public List<Series> series = new ArrayList<Series>();// 纵坐标

    public EchartsTree(Tooltip tooltip, List<Series> series) {
        this.tooltip = tooltip;
        this.series = series;
    }

    public Tooltip getTooltip() {
        return tooltip;
    }

    public void setTooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
    }

    public List<Series> getSeries() {
        return series;
    }

    public void setSeries(List<Series> series) {
        this.series = series;
    }
}
