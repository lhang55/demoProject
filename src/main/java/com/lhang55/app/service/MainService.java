package com.lhang55.app.service;

import com.zzy.db.DB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/4/9 15:11
 */
@Service
public class MainService {

    public String talk(String input) {
        input = input.replace("吗", "");
        input = input.replace("?", "!");
        input = input.replace("？", "!");
        input = input.replace("? ", "!");
        return input;
    }

    public String talk2(String input){
        input = input.replace("我","我也");
        return input;
    }

}
