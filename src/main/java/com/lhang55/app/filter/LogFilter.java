package com.lhang55.app.filter;

import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;

public class LogFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String site = filterConfig.getInitParameter("Site");
        System.out.println("网站初始化名称:" + site);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String name = servletRequest.getParameter("name");
        System.out.println("过滤器获取请求参数:" + name);
        System.out.println("logFilter过滤器执行--网站名称：http://www.lhang55.com");
        if ("lhang".equals(name)) {
            // 把请求传回过滤链
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            //设置返回内容类型
            servletResponse.setContentType("text/html;charset=GBK");

            //在页面输出响应信息
            PrintWriter out = servletResponse.getWriter();
            out.print("<b>name不正确，请求被拦截，不能访问web资源</b>");
            System.out.println("name不正确，请求被拦截，不能访问web资源");
        }
    }

    @Override
    public void destroy() {

    }
}
