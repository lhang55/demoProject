package com.lhang55.app.version.bean;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/8/23 21:31
 */
@Data
@Table(name = "app_version")
public class AppVersion {
    @Id
    private Long versionId;
    @Column
    private String versionNumber;
    @Column
    private String filePath;
    @Column
    private Long creatorId;
    @Column
    private Date createdate;
}
