package com.lhang55.app.controller;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

@WebServlet(name = "MyServlet", urlPatterns = "/myservlet")
public class MyServlet extends HttpServlet {

    private String message;

    @Override
    public void init() throws ServletException {
        super.init();
        message = "Hello World";
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Locale locale = request.getLocale();
        String language = locale.getLanguage();
        String country = locale.getCountry();
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("content-language", "es");
        PrintWriter out = response.getWriter();

        ServletContext servletContext = this.getServletContext();
        servletContext.setAttribute("name","lhang55");

        String title = "监测区域设置";
        String docType = "<!DOCTYPE html>\n";
        out.println(docType +
                "<html>\n" +
                "<head><title>" + title + "</title></head>\n" +
                "<body bgcolor=\"#f0f0f0\">\n" +
                "<h1 align=\"center\">" + language + "</h1>\n" +
                "<h1 align=\"center\">" + country + "</h2>\n" +
                "<h1 align=\"name\">lhang55</h2>\n" +
                "</body></html>");
    }

    @Override
    public void destroy() {

    }
}
