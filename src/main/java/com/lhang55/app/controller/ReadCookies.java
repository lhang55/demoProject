package com.lhang55.app.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;

@WebServlet(name = "ReadCookies", urlPatterns = "/cookie/read")
public class ReadCookies extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public ReadCookies() {
        super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String docType = "<!DOCTYPE html>\n";
        out.println(docType + "<html>\n" +
                "<head><title>标题党</title></head>\n" +
                "<body bgcolor=\"#f0f0f0\">\n");
        if (null != cookies) {
            out.println("<h2>cookie 名称和值</h2>");
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if (cookie.getName().compareTo("name") == 0) {
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                    out.println("已删除的cookie:" + cookie.getName() + "<br/>");
                }
                out.println("名称：" + cookie.getName() + ",");
                out.println("最大：" + cookie.getMaxAge() + ", ");
                out.println("值：" + URLDecoder.decode(cookie.getValue(), "utf-8") + "<br/>");
            }
        } else {
            out.println("<h2 class=\"tutheader\">no cookie found</h2>");
        }
        out.println("</body>");
        out.println("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
