package com.lhang55.app.controller;

import com.google.gson.*;
import com.lhang55.app.bean.*;
import com.lhang55.app.config.CDPlayer;
import com.lhang55.app.vo.Resources;
import com.lhang55.app.vo.ResultVO;
import com.lhang55.app.service.MainService;
import com.lhang55.util.MD5Util;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.*;
import org.springframework.data.solr.core.query.result.HighlightEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/3/19 10:30
 */
@Controller
public class MainController {
    @Autowired
    private MainService mainService;

    @Autowired
    private SolrTemplate solrTemplate;

    @Autowired
    private CDPlayer cdPlayer;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView loadMainPage(HttpServletRequest request) throws Exception {
        ModelAndView mv = new ModelAndView("main.jsp");
        return mv;
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ModelAndView uploadNewVersionPatch(@RequestParam(value = "file") CommonsMultipartFile file, @RequestParam(value = "versionNumber") String versionNumber,
                                              RedirectAttributes redirectAttributes) {
        DateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
        try {
            //设置文件路径，生成文件对象,写入磁盘
            int pos = file.getOriginalFilename().lastIndexOf(".");
            String extendName = file.getOriginalFilename().substring(pos);
            String newFileName = format.format(new Date()) + "_" + MD5Util.getFileMD5String(file.getInputStream()) + extendName;
            String filePath = "D:/version-apk" + File.separator + "app_patch" + File.separator + newFileName;
            File f = new File(filePath);
            if (!f.getParentFile().exists()) {
                f.getParentFile().mkdirs();
            }
            FileUtils.writeByteArrayToFile(f, file.getBytes());

        } catch (Exception e) {
            e.printStackTrace();
            redirectAttributes.addFlashAttribute("errormsg", e.getMessage());
        }
        ModelAndView modelAndView = new ModelAndView("appVersionManage.jsp");
        return modelAndView;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ModelAndView search(@RequestParam("keywords") String keywords, HttpServletRequest request) throws Exception {
        List<ResultVO> resultVOList = new ArrayList<>();
        List<HighlightEntry.Highlight> highlights = new ArrayList<>();
        List<HighlightEntry<Resources>> highlightEntryList = new ArrayList<>();
        try {
            if (!keywords.equals("") && !keywords.isEmpty()) {
                Criteria criteria = new Criteria("text").expression(keywords);

                HighlightOptions highlightOptions = new HighlightOptions();
                highlightOptions.setFragsize(500);
                highlightOptions.setSimplePrefix("<font color='red'>");
                highlightOptions.setSimplePostfix("</font>");
                HighlightQuery query = new SimpleHighlightQuery(criteria);
                query.setHighlightOptions(highlightOptions);
                HighlightPage<Resources> page = solrTemplate.queryForHighlightPage("blog", query, Resources.class, org.springframework.data.solr.core.RequestMethod.GET);
                highlightEntryList = page.getHighlighted();
                for (HighlightEntry<Resources> highlightEntry :
                        highlightEntryList) {
                    ResultVO resultVO = new ResultVO();
                    resultVO.setResourceId(highlightEntry.getEntity().getResourceId());
                    resultVO.setContent(highlightEntry.getEntity().getContent());
                    highlights = highlightEntry.getHighlights();
                    resultVO.setContent(highlights.get(0).getSnipplets().get(0));
                    resultVOList.add(resultVO);
                }
                request.setAttribute("keywords", keywords);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("resultList", resultVOList);
        ModelAndView mv = new ModelAndView("main");
        return mv;
    }

    @RequestMapping(value = "/talk", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public Object talk(@RequestBody String input) {
        String result = input;
        if (input.contains("吗") || input.contains("?")) {
            result = mainService.talk(input);
        } else if (input.contains("我")) {
            result = mainService.talk2(input);
        }
        return result;
    }

    /**
     * app管理页面
     *
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/manage", method = RequestMethod.GET)
    public ModelAndView manage(HttpServletRequest request) throws Exception {
        ModelAndView mv = new ModelAndView("appVersionManage.jsp");
        return mv;
    }

    @RequestMapping(value = "/echarts", method = RequestMethod.GET)
    public String echarts(ModelMap model) {
        model.addAttribute("msg", "Spring MVC Hello World");
        return "echarts.jsp";
    }

    @RequestMapping(value = "/cookie", method = RequestMethod.GET)
    public ModelAndView cookie() {
        ModelAndView mv = new ModelAndView("cookie.html");
        return mv;
    }

    @RequestMapping(value = "/getDynmicLineData", method = RequestMethod.POST)
    //添加该注释后，返回值将由转换器进行转换，转换器为Jackson，所以会转换成json格式
    public @ResponseBody
    EchartData getDynmicLineData() {
        //数据分组
        List<String> legend = new ArrayList<String>(Arrays.asList(new String[]{"最高气温"}));
        //横坐标
        List<String> category = new ArrayList<String>(Arrays.asList(new String[]{"周一", "周二", "周三", "周四", "周五", "周六", "周日"}));
        //纵坐标
        List<Series> series = new ArrayList<Series>();

        Random random = new Random();
        int rand = random.nextInt();
        ArrayList<Long> temp = new ArrayList<Long>();

        for (int i = 0; i < 7; i++) {
            rand = Math.abs(random.nextInt() % 50);
            temp.add((long) rand);
        }
        Label label = new Label();
        series.add(new Series("flare", "tree", temp, label));
        EchartData data = new EchartData(legend, category, series);
        return data;
    }

    @RequestMapping(value = "/getTree", method = RequestMethod.GET)
    public @ResponseBody
    EchartsTree getTree() {
        Tooltip tooltip = new Tooltip("item", "mousemove");
        Label label = new Label("left", "middle", "right", "15");
        //纵坐标
        List<Series> seriesList = new ArrayList<Series>();

        JsonParser parser = new JsonParser();
        List<TreeNode> treeNodes = new ArrayList<TreeNode>();
        try {
            //创建一个JSON对象，接收parser解析后的返回值
            //使用parse()方法，传入一个Reader对象，返回值是JsonElement类型
            //因为要读取文件，所以传入一个FileReader
            //JsonObject是JsonElement的子类，所以需要强转
            //有异常抛出，使用 try catch 捕获
            JsonObject object = (JsonObject) parser.parse(new FileReader("D:\\data1.json"));
            //先将两个外部的属性输出 category 和 pop
            //先 get 到名称（键），返回的是 JsonElement，再 getAs 转换成什么类型的值
            //依据 json 格式里的数据类型
            TreeNode root = jsonFileToObject(object, "flare");
            treeNodes.add(root);
        } catch (JsonIOException e) {
            e.printStackTrace();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Series series = new Series("", "tree", treeNodes, label);
        series.setTop("5%");
        series.setRight("5%");
        series.setBottom("2%");
        series.setLeft("5%");
        seriesList.add(series);
        EchartsTree data = new EchartsTree(tooltip, seriesList);
        return data;
    }


    /**
     * 将jsonObject转化为object对象
     *
     * @param object
     * @param pid
     * @return
     */
    public TreeNode jsonFileToObject(JsonObject object, String pid) {
        //先将两个外部的属性输出 category 和 pop
        //先 get 到名称（键），返回的是 JsonElement，再 getAs 转换成什么类型的值
        //依据 json 格式里的数据类型
        TreeNode treeNode = null;
        if (object.has("name")) {
            treeNode = new TreeNode(object.get("name").getAsString());
            treeNode.setId(object.get("name").getAsString());
            treeNode.setPid(pid);
//            System.out.println("name:" + object.get("name").getAsString());
        }
        if (object.has("value")) {
            treeNode.setValue(object.get("value").getAsString());
//            System.out.println("value:" + object.get("value").getAsString());
        }

        if (object.has("children")) {
            JsonArray array = object.get("children").getAsJsonArray();
            for (int i = 0; i < array.size(); i++) {
                if (array.get(i).getAsJsonObject() != null) {
                    treeNode.add(jsonFileToObject(array.get(i).getAsJsonObject(), treeNode.getId()));
                }
            }
        }
        return treeNode;
    }
}
