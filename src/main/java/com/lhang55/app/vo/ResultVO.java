package com.lhang55.app.vo;

import lombok.Data;

import java.util.Date;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/4/16 18:11
 */
@Data
public class ResultVO {
    private String resourceId;

    private String content;

    private Date createdate;
}
