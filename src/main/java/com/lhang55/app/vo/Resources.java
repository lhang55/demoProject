package com.lhang55.app.vo;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/4/12 10:57
 */
@Data
@SolrDocument(solrCoreName = "blog")
public class Resources {
    @Id
    @Field
    private String resourceId;

    @Field
    private String content;

    @Field
    private Date createdate;
}
