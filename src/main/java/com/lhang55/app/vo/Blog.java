package com.lhang55.app.vo;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.util.List;

/**
 * @Description:
 * @Author: lihang
 * @CreateDate: 2018/4/10 13:41
 */
@SolrDocument(solrCoreName = "solrTest")
public class Blog{
    @Id
    @Field
    private String id;

    @Field
    private String url;

    @Field
    private List<String> title;

    @Field
    private String remark;

    @Field
    private List<String> content;

    public Blog() {
    }

    public Blog(String id, String url, List<String> title, String remark, List<String> content) {
        this.id = id;
        this.url = url;
        this.title = title;
        this.remark = remark;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getTitle() {
        return title;
    }

    public void setTitle(List<String> title) {
        this.title = title;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }
}
