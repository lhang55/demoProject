package com.lhang55.app.lambda;

/**
 * @Description:
 * @Author: lhang55
 * @CreateDate: 2019/1/14 17:51
 */
@FunctionalInterface
public interface WorkerInterface {
    /**
     * 打印函数
     */
    void doSomeWork();
}
