<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.lhang55.app.vo.Resources" %>
<%@ page import="com.lhang55.app.vo.ResultVO" %>
<%@ page import="com.lhang55.app.bean.HumorBlog" %><%--
  Created by IntelliJ IDEA.
  User: lh
  Date: 2018/4/13
  Time: 9:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<%
    String contextPath = request.getContextPath();
    String baseurl = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
    request.setAttribute("basePath", baseurl);
%>
<html>
<head>
    <meta charset="utf-8">
    <title>搜索</title>
    <link href="${basePath}/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${basePath}/static/css/animate.css" rel="stylesheet">
    <link href="${basePath}/static/css/style.css" rel="stylesheet">

    <style>
        /*dl {*/
        /*display: block;*/
        /*-webkit-margin-before: 1em;*/
        /*-webkit-margin-after: 1em;*/
        /*-webkit-margin-start: 0px;*/
        /*-webkit-margin-end: 0px;*/
        /*}*/
        body {
            margin: 0px;
            padding: 0px;
            font-size: 14px;
            color: #333;
        }

        dl.list {
            border-bottom: 1px dashed #ccc;
            line-height: 1.8em;
            padding: 10px 15px 10px;
        }

        dl.list dt {
            display: block;
            height: 30px;
        }

        /*.content-dd {*/
        /*display: block;*/
        /*}*/
    </style>
</head>
<body>


<div class="container">
    <div class="animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="search-form">
                            <form method="get" action="${basePath}/search">
                                <div class="input-group">
                                    <input type="text" placeholder="请输入关键词" id="keywords" name="keywords"
                                           class="form-control input-lg">
                                    <div class="input-group-btn">
                                        <input value="搜 索" class="btn btn-lg btn-primary" id="search-submit"
                                               type="submit"/>
                                    </div>
                                </div>
                                <c:if test="${keywords != null&&keywords!=''}">
                                    <h4>
                                        <span class="text-navy">"${keywords}"</span>&nbsp;<a href="${basePath}"><span
                                            style="color: red">x</span></a>
                                    </h4>
                                </c:if>
                            </form>
                        </div>
                        <c:forEach items="${resultList}" var="result" varStatus="status">
                            <%ResultVO result = (ResultVO) pageContext.getAttribute("result"); %>
                            <div class="hr-line-dashed"></div>
                            <div class="search-result row" style="margin-left: 5px">

                                <div>
                                    <dl class="list">
                                        <dt>
                                            <span style="color: red">
                                                    ${result.createdate}
                                            </span>
                                        </dt>
                                        <dd class="content-dd" style="text-indent: 0">
                                                ${result.content}
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Mainly scripts -->
<script src="${basePath}/static/js/jquery-2.1.1.js"></script>
<script src="${basePath}/static/js/jquery.form.js"></script>
<script src="${basePath}/static/js/bootstrap.js"></script>

</body>

<script>
    var keywords = '${keywords}';
    $(document).ready(function () {
        $("div[name='labelDiv']").each(function () {
            var height = $(this).height();
            if (height > 40) {
                if (!keywords) {
                    $(this).height(30);
                    $(this).addClass("dropdown");
                } else {
                    $(this).css("height", "100%");
                    $(this).addClass("dropup");
                }

                $(this).append("<a onclick='resetHeight(this)'> <i class='caret'></i></a>");
            }
        });
    });

    function resetHeight(obj) {
        var labelDiv = $(obj).parents("[name='labelDiv']");
        console.log(labelDiv);
        if (labelDiv.height() > 40) {
            labelDiv.height(30);
            labelDiv.removeClass("dropup");
            labelDiv.addClass("dropdown");
        } else {
            labelDiv.css("height", "100%");
            labelDiv.removeClass("dropdown");
            labelDiv.addClass("dropup");
        }
    }

</script>
</html>
