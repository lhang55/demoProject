<%--
  Created by IntelliJ IDEA.
  User: lh
  Date: 2018/4/2
  Time: 9:25
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%
    String contextPath = request.getContextPath();
    String baseurl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + contextPath + "/";
    request.setAttribute("basePath", baseurl);
%>
<html>
<head>
    <title>${msg}</title>
</head>
<script src="${basePath}/static/js/jquery-2.1.1.js"></script>
<script src="${basePath}/static/js/echarts.js"></script>
<body>

<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="height:800px"></div>


<script>
    var myChart;
    $(document).ready(function () {
        myChart = echarts.init(document.getElementById('main'));
        //创建ECharts图表方法
        myChart.showLoading({
            text: "图表数据正在努力加载..."
        });
        var options = {
            title: {
                text: "",
                align: 'center',
                subtext: "",
                sublink: ""
            },
            tooltip: {},
            legend: {
                data: []
            },
            calculable: true,
            series: []
        };
        myChart.setOption(options); //先把可选项注入myChart中
        getChartData();
    });
</script>


<script>
    function getChartData() {
        //获得图表的options对象
        var options = myChart.getOption();
        //通过Ajax获取数据
        $.ajax({
            type: "get",
            async: false, //同步执行
            url: "/getDynmicLineData",
            data: {},
            dataType: "json", //返回数据形式为json
            success: function (result) {
                if (result) {
                    options.tooltip = result.tooltip;
                    options.series = result.series;

                    myChart.hideLoading();
                    myChart.setOption(options);
                }
            },
            error: function (errorMsg) {
                alert("图表请求数据失败!");
                myChart.hideLoading();
            }
        });
    }
</script>
</body>
</html>
