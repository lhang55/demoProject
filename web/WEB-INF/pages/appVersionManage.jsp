<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%
    String contextPath = request.getContextPath();
    String baseurl = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
    request.setAttribute("baseurl", baseurl);
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="robots" content="none"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>APP版本管理</title>
    <meta name="renderer" content="webkit">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="${baseurl}/static/css/bootstrap.min.css">
    <link rel="stylesheet" href="${baseurl}/static/css/font-awesome.min.css">
    <link rel="stylesheet" href="${baseurl}/static/css/font-material.css">
    <link rel="stylesheet" href="${baseurl}/static/css/ionicons.min.css">

</head>
<body>
<div class="container">
    <div class="row" style="height: 20px;"></div>
    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">上传新版本</button>
        </div>
    </div>
    <div class="row" style="height: 8px;"></div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" enctype="multipart/form-data" action="${baseurl}upload">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">上传文件</h4>
                    </div>
                    <div class="modal-body">
                            <div class="form-group">
                                <label for="versionNumber">版本号</label>
                                <input type="text" name="versionNumber" class="form-control" id="versionNumber" placeholder="版本号">
                            </div>
                            <div class="form-group">
                                <label for="file">更新包</label>
                                <input type="file" name="file" id="file">
                                <p class="help-block">请选择要上传的更新资源包</p>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-primary">确定</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<script src="${baseurl}/static/js/jquery-2.1.1.js"></script>
<script src="${baseurl}/static/js/bootstrap.js"></script>
</body>
</html>
